import {SafeAreaView, StyleSheet} from 'react-native';
import JougouteScreen from "./screens/JougouteScreen";
import AppLoading from "./screens/AppLoading";
import {useEffect, useMemo, useState} from "react";
import * as Crypto from "expo-crypto";
import {CryptoDigestAlgorithm} from "expo-crypto";
import * as SecureStore from "expo-secure-store";
import {instance, JougouteContext} from "./auth";
import {StatusBar} from "expo-status-bar";

export default function App() {

  const [loading, setLoading] = useState<boolean>(true);
  const [auth, setAuth] = useState<string|undefined>();
  const [racine, setRacine] = useState<MetadataFolder<MetadataFolder<MetadataImage>>|undefined>();

  const jougouteValue = useMemo(() => (
      {
          auth: auth,
          racine: racine
      }
  ), [auth, racine])

  async function login(): Promise<boolean> {
      const digest = await instance.get("getdigest").then(res => res.data.digest);
      const email = await Crypto.digestStringAsync(CryptoDigestAlgorithm.SHA1, 'email'.toLowerCase());
      const pdigest = await Crypto.digestStringAsync(CryptoDigestAlgorithm.SHA1, 'password' + email + digest);
      const params = "&username=email&digest="+digest+"&passworddigest="+pdigest;
      const result = await instance.get("userinfo?getauth=1"+params).then(res => {
          return res.data.auth;
      }).catch(err => {
          console.log(err);
      })

      if(result) {
          await SecureStore.setItemAsync("auth", result);
          setAuth(result);
          setRacine(undefined);
          return true;
      }
      return false;
  }

  useEffect(() => {
    (
        async () => {
            let valid: boolean;
            //Verification du token dans le secure store
            const auth = await SecureStore.getItemAsync("auth");
            if(auth) {
                valid = true;
                setAuth(auth);
            } else {
                //Tentative de connexion pour le token
                valid = await login();
            }

            if(valid) {
                //On récupère notre racine pour n'avoir que le dossier "Jougoute"
                await instance.get("listfolder?folderid=0&auth="+auth)
                    .then(r => {
                        const data = r.data;
                        const folders: MetadataFolder<MetadataFolder<MetadataImage>>[] = data.metadata.contents;
                        //Filtre sur les dossiers pour ne récupérer que "Jougoute"
                        const folder = folders.find(f => f.name.toLowerCase() == "jougoute");
                        if(folder) {
                            setRacine(folder);
                        }
                    })
                    .catch(e => {
                        console.log(e);
                    })

                setTimeout(() => {
                    setLoading(false);
                }, 2500);
            }
        }
    )();
  }, []);

  if(loading) {
    return <AppLoading text={"Chargement du cloud"}/>
  }

  return (
      <JougouteContext.Provider value={jougouteValue}>
        <SafeAreaView style={styles.container}>
          <JougouteScreen />
        </SafeAreaView>
        <StatusBar hidden={false} translucent={true} style={"dark"} backgroundColor={"#ffe4ad"}/>
      </JougouteContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffe4ad',
    padding: 24,
    flex: 1
  },
});

