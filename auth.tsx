//EAPI pour l'europe, API pour les Etats-Unis
import axios from "axios";
import {createContext} from "react";

export const instance = axios.create({ baseURL: "https://eapi.pcloud.com/", responseType: "json", responseEncoding: "utf-8", timeout: 10000});

export const JougouteContext = createContext<any>({});
