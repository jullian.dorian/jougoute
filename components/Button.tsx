import React from 'react';
import {StyleSheet, Text, TouchableHighlight, View} from "react-native";

function Button(props: any) {
    return (
        <TouchableHighlight onPress={props.action} style={props.disabled ? ButtonStyle.disabled : ButtonStyle.touchable} underlayColor={"#ffe4ad"} disabled={props.disabled} >
            <Text style={ButtonStyle.text}>Jougouter !</Text>
        </TouchableHighlight>
    );
}

const ButtonStyle = StyleSheet.create({
    touchable: {
        backgroundColor: "#ffd164",
        paddingHorizontal: 32,
        paddingVertical: 16,
        borderRadius: 12,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    disabled: {
        opacity: 0.5,
        backgroundColor: "#93835d",
        paddingHorizontal: 32,
        paddingVertical: 16,
        borderRadius: 12,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    text: {
        fontSize: 16,
        textTransform: "uppercase",
        fontWeight: "bold"
    }
});

export default Button;
