import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

function AppLoading(props: { text: string }) {
    return (
        <View style={AppLoadingStyle.container}>
            <Image source={require('../assets/icon.png')} resizeMode={"contain"} style={AppLoadingStyle.picture}/>
            <Text style={AppLoadingStyle.text}>{props.text}</Text>
        </View>
    );
}

const AppLoadingStyle = StyleSheet.create({
    container: {
      backgroundColor: '#ffe4ad',
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    },
    picture: {
        width: 256,
        height: 256
    },
    text: {
        fontSize: 16,
        marginTop: 32,
        fontWeight: "bold"
    }
});

export default AppLoading;
