import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import Button from "../components/Button";
import {instance, JougouteContext} from "../auth";
import AppLoading from "./AppLoading";
import { Audio } from 'expo-av';

function JougouteScreen() {

    const [loading, setLoading] = useState<boolean>(false);
    const [theme, setTheme] = useState<any>();
    const [character, setCharacter] = useState<MetadataImage>();
    const [song, setSong] = useState<Audio.Sound>();

    const jougoute = useContext(JougouteContext);

    const findRecursive = async (files: MetadataFolder<MetadataImage>[]) => {
        let find = false;
        let selected = files[Math.floor(Math.random() * files.length)];
        let max = 4;
        do {
            if(selected) {
                await instance.get("listfolder?folderid=" + selected.folderid + "&auth=" + jougoute?.auth)
                    .then(async p => {
                        const pictures: MetadataImage[] = p.data.metadata.contents;
                        if (pictures.length > 0) {
                            setTheme(selected);
                            const chara = pictures[Math.floor(Math.random() * pictures.length)];

                            //On récupère la thumbnail de l'image
                            await instance.get("getfilelink?fileid="+chara.fileid+"&auth="+jougoute?.auth)
                                .then(i => {
                                    chara.picture = "https://"+i.data.hosts[0] + i.data.path;
                                    setCharacter(chara);
                                })
                                .catch(e => {
                                    console.log(e);
                                })

                            find = true;
                        }
                    })
                    .catch(e => {
                        console.log(e);
                    });
                max--;
            }
        } while ((selected = files[Math.floor(Math.random() * files.length)]) && !find && max > 0);
    }

    const handleJougoute = async () => {
        await playSound();
        setLoading(true);


        //on récupère les fichiers
        await instance.get("listfolder?folderid="+jougoute?.racine?.folderid+"&auth="+jougoute?.auth)
            .then(async r => {
                const files: MetadataFolder<MetadataImage>[] = r.data.metadata.contents;

                await findRecursive(files);
                setLoading(false);

            })
            .catch(e => {
                console.log(e);
                setLoading(false);
            });
    }

    const playSound = async () => {
        const {sound} = await Audio.Sound.createAsync(require('../assets/son_fart.mp3'));
        setSong(sound);
        await sound.playAsync();
    }

    useEffect(() => {
        return song ? () => {
            song?.unloadAsync()
        } : undefined
    }, [song])

    if(loading) {
        return <AppLoading text={"Jougoute en cours"} />
    }

    return (
        <View style={JougouteScreenStyle.container}>
            {
                theme && character ?
                    (
                        <>
                            <Image style={JougouteScreenStyle.picture} source={{uri: character.picture}} resizeMode={"cover"} />
                            <View style={JougouteScreenStyle.header}>
                                <Text style={JougouteScreenStyle.title}>{character.name.replace(/\.[^/.]+$/, "")}</Text>
                                <Text style={JougouteScreenStyle.category}>{theme.name}</Text>
                            </View>
                        </>
                    ) :
                    (
                        <Text style={JougouteScreenStyle.infos}>Appuis sur le bouton pour trouver ton personnage.</Text>
                    )
            }

            <Button action={() => handleJougoute()} disabled={loading}/>
        </View>
    );
}

const JougouteScreenStyle = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    picture: {
        width: 256,
        height: 256,
        borderRadius: 16
    },
    header: {
        marginVertical: 42
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
    },
    category: {
        fontSize: 14,
        textAlign: "center"
    },
    infos: {
        marginBottom: 16,
        fontSize: 18,
        textAlign: "center",
        fontWeight: "bold"
    }
});

export default JougouteScreen;
