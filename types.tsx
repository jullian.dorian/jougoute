interface Metadata {
    comments: number,
    created: Date,
    folderid: number,
    icon: string,
    id: string,
    isfolder: boolean,
    ismine: boolean,
    isshared: boolean,
    modified: Date,
    name: string,
    parentfolderid: number,
    thumb: boolean
}

interface MetadataFolder<T extends Metadata> extends Metadata {
    contents: T[],
}

interface MetadataImage extends Metadata{
    category: number,
    contenttype: string,
    exifdatetime: Date,
    fileid: number,
    hash: number,
    height: number,
    size: number,
    width: number,
    picture: string|undefined
}


interface JougouteType {
    auth: string|undefined, racine: MetadataFolder<MetadataFolder<MetadataImage>>|undefined
}
